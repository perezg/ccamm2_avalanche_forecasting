# Danger level models trained with SNOWPACK version developed in November 2021:


"RF1.1.sav": Model trained with danger level forecast in the public bulletin (1997-2019)

"RF2.1.sav" Model trained with danger level tidy ('verified').
  
### Input features for these models:

set3_smet = ["HN24_mean",
             "HN24_mean_7d",
             "HN72_24_mean",
             "HS_mod_mean",
             "ILWR_mean",
             "ISWR_diff_mean",
             "LWR_net_mean",
             "MS_Snow_mean",
             "MS_Water_mean",
             "OLWR_mean",
             "OSWR_mean",
             "Qg0_mean",
             "Qs_mean",
             "Qw_mean",
             "RH_mean",
             "S4_mean",
             "S5_mean",
             "Sn_mean",
             "Ss_mean",
             "TA_mean",
             "VW_drift_mean",
             "VW_mean",
             "pAlbedo_mean",
             "wind_trans24_mean",
             "wind_trans24_mean_3d",
             "wind_trans24_mean_7d",
             "zSn_mean"]

set3_prof = [
    "min_ccl_pen",
    "pen_depth",
    "sn38_weak_100",
]

set3 = [
    *set3_smet,
    *set3_prof,
]

