# Danger level models trained with SNOWPACK version developed in November 2020:


"RF_all_1.sav": Model trained with danger level forecast in the public bulletin (1997-2019)

 "RF_tidy_1.sav" Model trained with danger level tidy ('verified').
  
### Input features for these models:

## Input features for models RF_all_1.sav & RF_tidy_1: 


set1_smet = [
    "HN24_mean",
    "HN24_mean_7d",
    "HN72_24_mean",
    "HS_mod_mean",
    "ILWR_mean",
    "ISWR_dir_mean",
    "LWR_net_mean",
    "MS_Snow_mean",
    "Qg0_mean",
    "Qs_mean",
    "Qw_mean",
    "RH_mean",
    "S4_mean",
    "Sn_mean",
    "Ss_mean",
    "TSS_mod_mean",
    "TA_mean",
    "VW_drift_mean",
    "VW_mean",
    "pAlbedo_mean",
    "wind_trans24_mean",
    "wind_trans24_mean_3d",
    "wind_trans24_mean_7d",
    "zSn_mean",
]


set_prof = [
    "crit_cut_length_weak_100",
    "min_ccl_pen",
    "pen_depth",
    "sn38_weak_100",
]


set1 = [
    *set1_smet,
    *set_prof,
]
