"""
@author: Martin Hendrick
"""
import pandas as pd
import numpy as np
import math

### hand_hardness reparam according to Monti 2014


def hhi_reparam(grain_type, rho, hhi_thres):
    g = hhi_thres.iloc[grain_type - 1].drop("grain_types")
    dist_dens = np.array(abs(g - rho))
    d = np.argmin(dist_dens)
    ind_g = g.index
    hhi = int(ind_g[d][0])
    if grain_type == 8:
        hhi = 5
    return hhi


### WL detections according to Monti 2014


def monti_2014_wl_detection(df):

    df["hand_hardness_sp"] = df["hand_hardness"]

    d = {
        "1_l": [0, 0, 0, 0, 0, 0, 0, 0, 0],
        "1_s": [143, 213, 189, 247, 287, 100, 213, 0, 259],
        "2_l": [144, 214, 190, 248, 288, np.nan, 214, 0, 260],
        "2_s": [np.nan, 268, 277, 319, 344, np.nan, 317, 0, 326],
        "3_l": [np.nan, 269, 278, 320, 345, np.nan, 318, 0, 327],
        "3_s": [np.nan, np.nan, 368, 400, np.nan, np.nan, 407, 0, 396],
        "4_l": [np.nan, np.nan, 369, 401, np.nan, np.nan, 408, 0, 397],
        "4_s": [np.nan, np.nan, 443, 518, np.nan, np.nan, 739, 0, 484],
        "5_l": [np.nan, np.nan, 444, 519, np.nan, np.nan, 740, 600, 485],
        "5_s": [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, 700, np.nan],
        "grain_types": ["PP", "DF", "RG", "FC", "DH", "SH", "MF", "ice", "FCxr"],
    }
    df_h = pd.DataFrame(data=d)
    df_h = df_h.replace(to_replace=np.nan, value=100000)
    hhi_thres = df_h

    df["grain_type_main"] = [int(str(x)[0]) for x in df.grain_type]
    df["grain_type_2"] = [int(str(x)[1]) for x in df.grain_type]
    df["crust"] = [int(str(x)[2]) for x in df.grain_type]
    df["crust"] = df["crust"] >= 2

    df["hand_hardness_gt1"] = df.apply(
        lambda row: hhi_reparam(row["grain_type_main"], row["density"], hhi_thres),
        axis=1,
    )
    df["hand_hardness_gt2"] = df.apply(
        lambda row: hhi_reparam(row["grain_type_2"], row["density"], hhi_thres), axis=1
    )

    df["hand_hardness"] = (df["hand_hardness_gt2"] + df["hand_hardness_gt1"]) / 2

    w = df.loc[df.crust == True]  # noqa

    df.loc[w.index, "hand_hardness"] = 5

    df = df.sort_values("depth")

    features = ["hand_hardness", "grain_size", "grain_type_main"]

    below_str = above_str = []
    for f in features:
        df[f + "_below"] = df[f]
        df[f + "_above"] = df[f].shift(1)
        below_str = below_str + [f + "_below"]
        above_str = above_str + [f + "_above"]

    diff_grain_size_thres = 0.4
    diff_hand_hardness_thres = 1.7

    grain_size_thres = 0.6
    hand_hardness_thres = 1.3

    ### interface criteria
    df["diff_grain_size"] = (
        1
        - df[["grain_size_below", "grain_size_above"]].min(axis=1)
        / df[["grain_size_below", "grain_size_above"]].max(axis=1)
        >= diff_grain_size_thres
    )
    df["diff_hand_hardness"] = (
        abs(df["hand_hardness_below"] - df["hand_hardness_above"])
        >= diff_hand_hardness_thres
    )
    df["depth_thres"] = df.depth <= 100

    ### layer below and layer above the interface criteria
    df["grain_size" + "_below"] = df["grain_size" + "_below"] >= grain_size_thres
    df["grain_size" + "_above"] = df["grain_size" + "_above"] >= grain_size_thres

    df["hand_hardness" + "_below"] = (
        abs(df["hand_hardness" + "_below"]) <= hand_hardness_thres
    )
    df["hand_hardness" + "_above"] = (
        abs(df["hand_hardness" + "_above"]) <= hand_hardness_thres
    )

    df["grain_type_main_below"] = ((df["grain_type_main_below"]) >= 4) & (
        (df["grain_type_main_below"]) <= 6
    )
    df["grain_type_main_above"] = ((df["grain_type_main_above"]) >= 4) & (
        (df["grain_type_main_above"]) <= 6
    )

    ### sum criteria for the layer below and above
    df["sum_lemons_layer_below"] = df[below_str].sum(axis=1)
    df["sum_lemons_layer_above"] = df[above_str].sum(axis=1)

    ### maximum between sum_lemons_layer_below and sum_lemons_layer_above
    df["sum_lemons_layer"] = df[
        ["sum_lemons_layer_below", "sum_lemons_layer_above"]
    ].max(axis=1)

    ### sum interface criteria with the layer criteria satifying the max number of criteria (the below or the above)
    df["sum_lemons_interface"] = df[
        ["depth_thres", "diff_grain_size", "diff_hand_hardness"]
    ].sum(axis=1)

    df["sum_lemons"] = df["sum_lemons_layer"] + df["sum_lemons_interface"]
    df["pwl"] = df["sum_lemons"] >= 5

    return df


# look for largest variations (the 3 largest one) among layers of stability indices 'sn38','ssi','crit_cut_length','sk38'
# compute pen_depth, crit_cut_length below pem_depth


def stab_feat(s):
    s = s.reset_index(drop=True)
    s["depth"] = s.height.max() - s.height
    max_height = s.height.max()
    feat = ["sn38", "ssi", "crit_cut_length", "sk38"]
    for f in feat:
        s[f + "_diff"] = s[f].diff()

    feat_col_diff = [
        "diff1",
        "diff2",
        "diff3",
        "diff1_depth",
        "diff2_depth",
        "diff3_depth",
        "diff1_val",
        "diff2_val",
        "diff3_val",
    ]
    G = pd.DataFrame(columns=feat_col_diff, index=feat)
    A = pd.DataFrame(
        columns=[
            "pen_depth",
            "min_ccl_pen",
            "min_ccl_pen_depth",
            "flag_pen",
            "max_height",
        ],
        index=[0],
    )
    A.max_height = max_height
    l = s.loc[(s.sk38 < 6) & (s.depth < 100)]  # noqa
    if len(l) > 4:  # 3
        A.flag_pen = 1
        pen_depth = l.depth.min()
        min_ccl_pen = l.crit_cut_length.min()
        min_ccl_pen_depth = l.loc[l.index == l.crit_cut_length.idxmin(), "depth"]

        for f in feat:
            nl = l[f + "_diff"].nlargest(3)
            w = list(nl)  # noqa
            nl_index = nl.index
            g = [
                list(l.loc[l.index == nl_index[0], "depth"])[0],
                list(l.loc[l.index == nl_index[1], "depth"])[0],
                list(l.loc[l.index == nl_index[2], "depth"])[0],
            ]
            g_val = [
                list(l.loc[l.index == nl_index[0], f])[0],
                list(l.loc[l.index == nl_index[1], f])[0],
                list(l.loc[l.index == nl_index[2], f])[0],
            ]
            G.loc[G.index == f, "diff1"] = list(nl)[0]
            G.loc[G.index == f, "diff2"] = list(nl)[1]
            G.loc[G.index == f, "diff3"] = list(nl)[2]

            G.loc[G.index == f, "diff1_depth"] = g[0]
            G.loc[G.index == f, "diff2_depth"] = g[1]
            G.loc[G.index == f, "diff3_depth"] = g[2]
            G.loc[G.index == f, "diff1_val"] = g_val[0]
            G.loc[G.index == f, "diff2_val"] = g_val[1]
            G.loc[G.index == f, "diff3_val"] = g_val[2]

        A.pen_depth = pen_depth
        A.min_ccl_pen = min_ccl_pen
        A.min_ccl_pen_depth = list(min_ccl_pen_depth)[0]
    else:
        A.pen_depth = s.depth.max()
        A.flag_pen = 0
    return G, A


### compute for features in list_feat the min and depth of WL layers in the top 100cm


def weakest_pwl(s, list_feat):
    s_pwl = s.loc[s.pwl == 1]
    w = [x + "_weak" for x in list_feat]
    depth = [x + "_depth" for x in list_feat]
    w_100 = [x + "_weak_100" for x in list_feat]
    depth_100 = [x + "_depth_100" for x in list_feat]
    l = w + depth + w_100 + depth_100  # noqa
    r = pd.DataFrame(columns=l, index=[0])
    if len(s_pwl) > 0:
        p = s_pwl.index
        ind_check = list(p) + list(p - 1) + list(p + 1)
        ind_check = list(set(ind_check))
        i = ind_check.copy()
        max_ind = max(s.index)
        for u in i:
            if u < 0:
                ind_check.remove(u)
            if u > max_ind:
                ind_check.remove(u)

        pwl_neib = s.iloc[ind_check]
        pwl_neib_100 = pwl_neib.loc[pwl_neib.depth < 101]

        # list_feat = ['ssi','sk38','sn38','crit_cut_length']

        for feat in list_feat:
            pwl_min = pwl_neib[feat].min()
            idmin = pwl_neib[feat].idxmin()
            j = pwl_neib.loc[pwl_neib.index == idmin]
            pwl_min_depth = j.depth.mean()  # .item()
            r[feat + "_weak"] = pwl_min
            r[feat + "_depth"] = pwl_min_depth
            if len(pwl_neib_100) > 0:
                pwl_min_100 = pwl_neib_100[feat].min()
                idmin_100 = pwl_neib_100[feat].idxmin()
                j_100 = pwl_neib_100.loc[pwl_neib_100.index == idmin_100]
                pwl_min_depth_100 = j_100.depth.mean()  # .item()
                r[feat + "_weak_100"] = pwl_min_100
                r[feat + "_depth_100"] = pwl_min_depth_100
            else:
                r[feat + "_weak_100"] = -999
                r[feat + "_depth_100"] = -999
    else:
        r[l] = -999
    return r


### generate features from profiles


def dry_feat_extract_opt(s):

    s = s.reset_index(drop=True)
    s["depth"] = s.height.max() - s.height  # depth = distance from the top layer

    pwl_100_p = s.loc[
        (s.pwl == 1) & (s.depth < 100)
    ]  # layer with pwl=1 and in the 100cm from the top
    if (
        len(pwl_100_p) > 0
    ):  # is there a pwl in the 100cm from the top? yes pwl_100 = 1 no pwl_100=0
        pwl_100 = 1
    else:
        pwl_100 = 0

    pwl_100_15_p = s.loc[(s.pwl == 1) & (s.depth < 100) & (s.depth > 15)]
    if (
        len(pwl_100_15_p) > 0
    ):  # is there a pwl in the 15-100cm from the top? yes pwl_100_15 = 1 no pwl_100_15=0
        pwl_100_15 = 1
    else:
        pwl_100_15 = 0

    s_pwl = s.loc[s.pwl == 1]
    min_depth_pwl = s_pwl.depth.min()  # depth of closest pwl from the top layer
    max_depth_pwl = s_pwl.depth.max()  # depth of the deepest pwl from the top layer
    mean_depth_pwl = s_pwl.depth.mean()  # mean depth of the pwl from the top layer
    s_100_15 = s.loc[(s.depth < 100) & (s.depth > 15)].copy()
    s_100_15["ccl_sk38"] = (
        s_100_15["sk38"] * s_100_15["crit_cut_length"]
    )  # experimental stability index: sk38*crit_cut_length in the 15-100cm
    min_ccl_sk38 = s_100_15[
        "ccl_sk38"
    ].min()  # min of sk38*crit_cut_length in the 15-100cm
    if len(s_100_15) > 0:
        min_ccl_sk38_ind = s_100_15["ccl_sk38"].idxmin()
        j = s_100_15.loc[s_100_15.index == min_ccl_sk38_ind]
        min_ccl_sk38_depth = (
            j.depth.mean()
        )  # .item()						#depth of the min of sk38*crit_cut_length in the 15-100cm
    else:
        min_ccl_sk38_depth = np.nan

    base = s.loc[(s.height < 30)]
    if len(base.loc[base.pwl == 1]) > 0:
        base_pwl = 1  # is there a pwl in the 30cm from the ground? yes: base_pwl = 1, no:base_pwl=0
    else:
        base_pwl = 0
    list_feat = ["ssi", "sk38", "sn38", "crit_cut_length"]
    r = weakest_pwl(s, list_feat)
    return (
        pwl_100,
        pwl_100_15,
        min_depth_pwl,
        min_ccl_sk38,
        base_pwl,
        min_ccl_sk38_depth,
        max_depth_pwl,
        mean_depth_pwl,
        r,
    )


def level_0_profile_extract_dry_live(pro, z):

    l_date = list(set(pro.datum))

    E = pd.DataFrame()
    E_diff = pd.DataFrame()
    for d in l_date:
        s = pro.loc[pro.datum == d].copy()
        s["depth"] = s.height.max() - s.height
        s_layer_lab = monti_2014_wl_detection(s)
        (
            pwl_100,
            pwl_100_15,
            min_depth_pwl,
            min_ccl_sk38,
            base_pwl,
            min_ccl_sk38_depth,
            max_depth_pwl,
            mean_depth_pwl,
            r,
        ) = dry_feat_extract_opt(s_layer_lab)
        l = [  # noqa
            "pwl_100",
            "pwl_100_15",
            "min_depth_pwl",
            "min_ccl_sk38",
            "base_pwl",
            "min_ccl_sk38_depth",
            "max_depth_pwl",
            "mean_depth_pwl",
        ]
        dry_feat = [
            pwl_100,
            pwl_100_15,
            min_depth_pwl,
            min_ccl_sk38,
            base_pwl,
            min_ccl_sk38_depth,
            max_depth_pwl,
            mean_depth_pwl,
        ]
        wet_dry = list(dry_feat)

        feat_profile_list = list(r)
        P = pd.DataFrame(columns=feat_profile_list + l, index=[0])
        P[l] = wet_dry
        P[list(r)] = r
        P["datum"] = list(set(s.datum))
        P["sation_code"] = z
        E = pd.concat([E, P])

        ### height_diff_features
        G, A = stab_feat(s)
        u_l = [[x + "_" + a for a in list(G)] for x in list(G.index)]
        l = u_l[0] + u_l[1] + u_l[2] + u_l[3]  # noqa

        h = np.array(G)
        H = np.concatenate([h[0], h[1], h[2], h[3]])
        P_diff = pd.DataFrame(H, index=list(range(36))).T
        P_diff.columns = l
        P_diff["datum"] = list(set(s.datum))
        P_diff["station_code"] = z
        P_diff = pd.concat([P_diff, A], axis=1)
        E_diff = pd.concat([E_diff, P_diff])

    E = E.reset_index(drop=True)
    E_diff = E_diff.reset_index(drop=True)
    # E_tot = pd.concat([E_diff,E],axis=0)
    return E, E_diff


### from output of read_profile call functions to extract features from proiles


def features_from_profile_datetime(Pro, name, date_time):

    R = []
    F = []

    Pro = pd.DataFrame.from_dict(Pro)
    Pro = Pro[6:].copy()
    Pro.index = pd.to_datetime(Pro.index)

    P = pd.DataFrame(
        columns=[
            "datum",
            "station_code",
            "pen_depth",
            "pen_depth_trick",
            "max_height",
            "crust_30",
        ],
        index=[0],
    )
    P.station_code = name
    pro = Pro.loc[Pro.index == date_time]
    pro = pro.data[0]
    pro.pop("stab_indices", None)
    pro.pop("sh_at_surface", None)
    pro = pd.DataFrame.from_dict(pro)
    if len(pro) > 0:

        # Q = pro
        pro = pro.sort_values("height", ascending=False).reset_index(drop=True)
        pro["depth"] = pro.height.max() - pro.height
        pro["layer_thickness"] = abs(pro.depth - pro.depth.shift(-1))
        pro["datum"] = date_time
        a, b = level_0_profile_extract_dry_live(pro, name)
        f = pd.concat([a[list(a)[:-2]], b], axis=1)
        if len(pro) > 3:
            pro.loc[pro.index == max(pro.index), "layer_thickness"] = pro.iloc[-2]["layer_thickness"].max()
        pro["layer_thickness_cum_sum"] = pro.layer_thickness.cumsum()
        pro["weighted_density"] = pro.density * pro.layer_thickness
        pro["weighted_density_cum_sum"] = pro["weighted_density"].cumsum()
        pro["pen_depth"] = (100 * 0.8 * 43.3) / (pro["weighted_density_cum_sum"] / pro["layer_thickness_cum_sum"])  # 100 to have cm
        if pro.sk38.mean() < 6:
            pen_depth_trick = pro.iloc[pro.loc[pro.sk38 < 6].index[0]][
                "depth"
            ]  # pro.iloc[pro.loc[pro.sk38<6].index[0]-1]['depth']
        else:
            pen_depth_trick = pro.height.max()

        pro.grain_type = pro.grain_type.astype("int")
        pro.loc[
            (pro.depth < 30)
            & (pro.grain_type == 772)
            & (pro.density > 500)
            & (pro.layer_thickness >= 3 / np.cos(math.radians(38))),"crust",] = 1
        crust_all = pro.loc[pro.crust == 1]
        if len(crust_all) > 0:
            crust_depth = crust_all.depth.min()
        else:
            crust_depth = 999
        P["pen_depth_trick"] = pen_depth_trick
        P_k = pro.loc[pro.depth < 30, "pen_depth"][-1:]
        P_k = P_k.max()
        if P_k > pro.height.max():
            P_k = pro.height.max()
        if (P_k) * 1 == np.nan:
            P_k = pro.height.max()
        if crust_depth < P_k:
            P_k = crust_depth
        no_slab = pro.loc[(pro.depth >= P_k) & (pro.depth < 100)]
        if len(no_slab) > 0:
            min_ccl_pen = no_slab.crit_cut_length.min()
            min_ccl_pen_depth = no_slab.loc[
                no_slab.crit_cut_length == min_ccl_pen, "depth"].min()
        else:
            min_ccl_pen = 4
            min_ccl_pen_depth = np.nan
        P["min_ccl_pen"] = min_ccl_pen
        P["min_ccl_pen_depth"] = min_ccl_pen_depth
        P["crust_30"] = len(crust_all)
        P["datum"] = date_time
        P["pen_depth"] = P_k
        P.max_height = pro.height.max()
        R = R + [P]
        F = F + [f]

    if len(R) > 0:
        R = pd.concat(R).reset_index(drop=True)
    else:
        R = []
    if len(F) > 0:
        F = pd.concat(F).reset_index(drop=True)
    else:
        F = []

    F = F.drop(
        ["pen_depth", "min_ccl_pen", "min_ccl_pen_depth", "flag_pen", "max_height"],
        axis=1,
    )
    E = pd.concat([R, F], axis=1)
    E = E.loc[:, ~E.columns.duplicated()]
    return E
