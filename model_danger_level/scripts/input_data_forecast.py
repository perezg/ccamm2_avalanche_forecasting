"""
Created: 11/2020
@author: Cristina Perez
"""
import numpy as np
import pandas as pd

from deapsnow_live import config
from deapsnow_live.models.rf_models.scripts.resample_smet_server_forecast import (
    read_resample_smet_forecast,
)
from deapsnow_live.profiles.utils import from_file as profiles_from_file
from deapsnow_live.models.rf_models.config.features import set4_smet, set4, set_prof
from deapsnow_live.models.rf_models.config.shared import station_list


def extract_input_forecast(
    in_path_forecast, in_path_nowcast, out_path, datetime_str, save
):
    """
    in_path_forecast = Path of the forecast dataset
    in_path_nowcast = Path of the nowcast dataset
    out_path = Path where the processed results should be saved
    datetime_st = forecast date (input validfrom date of the forecast) %Y-%m-%d %H:%M:%S
    save = 'yes' (save the resampled input data of the forecast, all features)
    """
    dtype = {
        "validfrom": "datetime64[ns]",
        "validto": "datetime64[ns]",
        "time_min": "datetime64[ns]",
        "time_max": "datetime64[ns]",
        "station_code": int,
        "station": str,
        **{i: np.float for i in set4},
    }
    df = pd.DataFrame(
        columns=["station_code", "station", "validfrom", "validto", "time_min", "time_max"] + set4,
    ).astype(dtype=dtype)
    df2 = pd.DataFrame()
    data = pd.DataFrame(
        columns=["station_code", "station", "validfrom", "validto", "time_min", "time_max"] + set4,
    ).astype(dtype=dtype)
    for i in range(len(station_list)):
        try:
            # Extract smet data
            data_smet = read_resample_smet_forecast(
                in_path_forecast
                + "/"
                + station_list[i]
                + config.forecast_smet_file_extension,
                in_path_nowcast
                + "/"
                + station_list[i]
                + config.nowcast_smet_file_extension,
                datetime_str,
            )
            smet = data_smet[["station_code", "validfrom", "validto", "time_min", "time_max"] + set4_smet]
            # Extract profile data
            datetime_prof = str(
                smet.loc[0, "validto"] - pd.Timedelta(hours=3)
            )  # Date & Time to extract the profile data
            data_prof = profiles_from_file(
                in_path_forecast
                + "/"
                + station_list[i]
                + config.forecast_pro_file_extension,
                datetime_prof,
            )
            data_prof.loc[
                data_prof["crit_cut_length_weak_100"] == -999,
                "crit_cut_length_weak_100",
            ] = 4
            data_prof.loc[data_prof["sn38_weak_100"] == -999, "sn38_weak_100"] = 6
            prof = data_prof[set_prof]
            data = pd.concat([smet, prof], axis=1)
            data.loc[0, "station"] = station_list[i]
            df = df.append(data, ignore_index=True)
            data_all = pd.concat([data_smet, data_prof], axis=1)
            df2 = df2.append(data_all, ignore_index=True)
        except:  # noqa
            data.loc[0, "validfrom"] = pd.Timestamp(datetime_str)
            data.loc[0, "validto"] = (
                pd.Timestamp(datetime_str)
                + pd.Timedelta(days=1)
                - pd.Timedelta(hours=3)
            )
            data.loc[0, "time_min"] = np.datetime64("NaT")
            data.loc[0, "time_max"] = np.datetime64("NaT")
            data.loc[0, set4_smet + set_prof] = np.nan
            data.loc[0, ["station_code", "station"]] = station_list[i]
            df = df.append(data, ignore_index=True)
    if save == "yes":
        df2.to_csv(
            out_path + "/" + datetime_str[0:10] + "_" + datetime_str[11:13] + ".csv"
        )
    else:
        pass
    return df


def extract_input_forecast_aspect(
    in_path_forecast, in_path_nowcast, out_path, datetime_str, aspect, save
):
    """
    in_path_forecast = Path of the forecast dataset
    in_path_nowcast = Path of the nowcast dataset
    out_path = Path where the processed results should be saved
    datetime_st = forecast date (input validfrom date of the forecast) %Y-%m-%d %H:%M:%S
    aspect = 'north', 'east', 'south', 'west'
    save = 'yes' (save the resampled input data of the forecast, all features)
    """
    dtype = {
        "validfrom": "datetime64[ns]",
        "validto": "datetime64[ns]",
        "time_min": "datetime64[ns]",
        "time_max": "datetime64[ns]",
        "station_code": int,
        "station": str,
        **{i: np.float for i in set4},
    }
    df = pd.DataFrame(
        columns=["station_code", "station", "validfrom", "validto", "time_min", "time_max"] + set4,
    ).astype(dtype=dtype)
    df2 = pd.DataFrame()
    data = pd.DataFrame(
        columns=["station_code", "station", "validfrom", "validto", "time_min", "time_max"] + set4,
    ).astype(dtype=dtype)
    a = ""
    for i in range(len(station_list)):
        if aspect == "north":
            a = "1"
        elif aspect == "east":
            a = "2"
        elif aspect == "south":
            a = "3"
        elif aspect == "west":
            a = "4"
        try:
            # Extract smet data
            data_smet = read_resample_smet_forecast(
                in_path_forecast
                + "/"
                + station_list[i]
                + a
                + config.forecast_smet_file_extension,
                in_path_nowcast
                + "/"
                + station_list[i]
                + a
                + config.nowcast_smet_file_extension,
                datetime_str,
            )
            smet = data_smet[["station_code", "validfrom", "validto", "time_min", "time_max"] + set4_smet]
            # Extract profile data
            datetime_prof = str(
                smet.loc[0, "validto"] - pd.Timedelta(hours=3)
            )  # Date & Time to extract the profile data
            data_prof = profiles_from_file(
                in_path_forecast
                + "/"
                + station_list[i]
                + a
                + config.forecast_pro_file_extension,
                datetime_prof,
            )
            data_prof.loc[
                data_prof["crit_cut_length_weak_100"] == -999,
                "crit_cut_length_weak_100",
            ] = 4
            data_prof.loc[data_prof["sn38_weak_100"] == -999, "sn38_weak_100"] = 6
            prof = data_prof[set_prof]
            data = pd.concat([smet, prof], axis=1)
            data.loc[0, "station"] = station_list[i] + a
            df = df.append(data, ignore_index=True)
            data_all = pd.concat([data_smet, data_prof], axis=1)
            df2 = df2.append(data_all, ignore_index=True)
        except:  # noqa
            data.loc[0, "validfrom"] = pd.Timestamp(datetime_str)
            data.loc[0, "validto"] = (
                pd.Timestamp(datetime_str)
                + pd.Timedelta(days=1)
                - pd.Timedelta(hours=3)
            )
            data.loc[0, "time_min"] = np.datetime64("NaT")
            data.loc[0, "time_max"] = np.datetime64("NaT")
            data.loc[0, set4_smet + set_prof] = np.nan
            data.loc[0, ["station_code", "station"]] = station_list[i] + a
            df = df.append(data, ignore_index=True)
    if save == "yes":
        df2.to_csv(
            out_path
            + "/"
            + datetime_str[0:10]
            + "_"
            + datetime_str[11:13]
            + "_"
            + aspect
            + ".csv"
        )
    else:
        pass
    return df
