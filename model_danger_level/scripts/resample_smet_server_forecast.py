# -*- coding: utf-8 -*-
"""
Created: 09/12/2020
1) Read station.smet or station.ext.smet data from a path.

2) Resample and computation of the mean of all the 'smet' features in a 24h window
(from 'validfrom' --> 'validto' dates):
      # validfrom = first recording date of the file (for example: '2020-11-24 00:00:00').
        Date & time of the forecast
      # validto = validto + 1 day (-3h) (for example: '2020-11-24 21:00:00',
        which is the last timestamp of a day (3h sampling)).

3) Computation of three new 'smet' feaures:
      # 'HN24_mean_7d': Sum of the 7 days, 24 hours mean of 'HN24'
      # 'wind_trans24_mean_7d': Sum of the 7 days, 24 hours mean of 'wind_trans24'
      # 'wind_trans24_mean_3d': Sum of the 3 days, 24 hours mean of 'wind_trans24'
@author: Cristina Perez
"""

import numpy as np
import pandas as pd
from pathlib import Path
import datetime

#from deapsnow_live import config
#from deapsnow_live.parsing.smet import read_smet

import config
from smet import read_smet


def compute_sum(fin, datetime_str):
    """
    fin = Path of the nowcast dataset
    datetime_st = forecast date (input validfrom date of the forecast)
    """
    if not isinstance(fin, Path):
        fin = Path(fin)
    df = read_smet(fin)

    # Computation of 2 and 6 days sum of daily mean of ['HN24', 'wind_trans24']
    date = datetime.datetime.strptime(datetime_str, config.date_time_format)
    df2 = df.copy()
    df2 = df2[df2["timestamp"] < date]
    df2[["datum", "time"]] = (
        df2["timestamp"].astype(str).str.split(" ", n=1, expand=True)
    )  # New colunms for resampling & Sum
    df2.loc[df2["time"] != df2["time"].iloc[-1], ["datum", "time"]] = np.nan
    df2[["datum", "time"]] = df2[["datum", "time"]].fillna(method="bfill")
    df2["datum"] = pd.to_datetime(df2["datum"])
    df2 = df2[["datum", "HN24", "wind_trans24"]]
    df3 = df2[["datum", "wind_trans24"]].copy()

    # Resampling 6 days (df2) & 2 days (df3):
    df2 = (
        df2[["datum", "HN24", "wind_trans24"]]
        .set_index("datum")
        .resample(rule="1d")
        .mean()
        .rolling(window="6d", min_periods=6, closed="right")
        .sum()
        .rename(
            columns={"HN24": "HN24_mean_6d", "wind_trans24": "wind_trans24_mean_6d"}
        )
    )

    df3 = (
        df3[["datum", "wind_trans24"]]
        .set_index("datum")
        .resample(rule="1d")
        .mean()
        .rolling(window="2d", min_periods=2, closed="right")
        .sum()
        .rename(columns={"wind_trans24": "wind_trans24_mean_2d"})
    )

    feat = pd.DataFrame(
        columns=["HN24_mean_6d", "wind_trans24_mean_6d", "wind_trans24_mean_2d"]
    )
    feat.loc[0, "HN24_mean_6d"] = df2["HN24_mean_6d"].iloc[-1]
    feat.loc[0, "wind_trans24_mean_6d"] = df2["wind_trans24_mean_6d"].iloc[-1]
    feat.loc[0, "wind_trans24_mean_2d"] = df3["wind_trans24_mean_2d"].iloc[-1]
    return feat


def read_resample_smet_forecast(fin1, fin2, datetime_str):
    """
    fin1 = Path of the forecast dataset
    fin2 = Path of the nowcast dataset
    datetime_st = forecast date (input validfrom date of the forecast)
    """
    if not isinstance(fin1, Path):
        fin1 = Path(fin1)
    df = read_smet(fin1)
    filename = fin1.name

    # Computation of the mean of all features in a 24h window
    # (from 'validfrom' - to 'validto' dates)
    # validfrom = df['timestamp'].min()
    # validto = (df['timestamp'].min() + pd.Timedelta(days=1) - pd.Timedelta(hours=3))
    validfrom = datetime.datetime.strptime(datetime_str, config.date_time_format)
    validto = validfrom + pd.Timedelta(days=1) - pd.Timedelta(hours=3)
    df_res = df.copy()  # df_res = resampled dataframe
    df_res = df_res[
        (df_res["timestamp"] >= validfrom) & (df_res["timestamp"] <= validto)
    ]
    df_res["validfrom"] = validfrom
    df_res["validto"] = validto
    time_min = df_res['timestamp'].min()
    time_max = df_res['timestamp'].max()
    df_res = df_res.groupby(["validfrom", "validto"]).agg(["mean"])
    df_res.columns = ["_".join(x) for x in df_res.columns.ravel()]
    df_res.reset_index(inplace=True)

    # Compute the 7 and 3 days sum() of ''HN24_mean' and 'wind_trans24_mean'
    df_feat = compute_sum(fin2, str(validfrom))  # fin2 is the Path of the nowcast data
    df_res["HN24_mean_7d"] = df_feat["HN24_mean_6d"] + df_res["HN24_mean"]
    df_res["wind_trans24_mean_7d"] = (
        df_feat["wind_trans24_mean_6d"] + df_res["wind_trans24_mean"]
    )
    df_res["wind_trans24_mean_3d"] = (
        df_feat["wind_trans24_mean_2d"] + df_res["wind_trans24_mean"]
    )
    df_res['time_min'] = time_min
    df_res['time_max'] = time_max
    df_res["station_code"] = filename[0:4]

    return df_res
