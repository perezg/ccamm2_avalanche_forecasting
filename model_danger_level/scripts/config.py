from datetime import datetime
import argparse
import os

date_time_format = "%Y-%m-%d %H:%M:%S"
#nowcast_smet_file_extension = ".ext.smet"  ### live set-up
nowcast_smet_file_extension = ".smet"
forecast_smet_file_extension = ".smet"
#nowcast_pro_file_extension = ".ext.pro"  ### live set-up
nowcast_pro_file_extension = ".pro"
forecast_pro_file_extension = ".pro"

path_prefix = os.environ.get("DEAPSNOW_PATH_PREFIX", ".")
current_season_official_danger_rating = (
    f"{path_prefix}/../live-data/actual_forecasts/bulletin_export_deapSnow.csv"
)
current_season_snowpack_data_dir_nowcast = (
    f"{path_prefix}/../live-data-revamp/snowpack-data-nowcast/"
)
current_season_snowpack_data_dir_forecast = (
    f"{path_prefix}/../live-data-revamp/snowpack-data-forecast/"
)
snowpack_data_server_url_nowcast = "http://snowpack.wd.op/share/snowpack-imis/output/"
snowpack_data_server_url_forecast = "http://snowpack.wd.op/share/snowpack-oshd/output/"
snowpack_swiss_metnet_url_nowcast = "http://snowpack.wd.op/share/snowpack-smn/output/"
preictions_summary_location = (
    f"{path_prefix}/../live-data-revamp/predictions/summaries/"
)

imis_stations = [
    "ALB2",
    "ALI2",
    "AMD2",
    "ANV2",
    "ANV3",
    "ARO2",
    "ARO3",
    "ATT2",
    "BANC2",
    "BED2",
    "BED3",
    "BED4",
    "BEL2",
    "BER2",
    "BER3",
    "BEV2",
    "BIN3",
    "BIN4",
    "BOG2",
    "BOR2",
    "BOV2",
    "BPIS2",
    "CAM2",
    "CAM3",
    "CHA2",
    "CMA2",
    "CON2",
    "CSL2",
    "DAV2",
    "DAV3",
    "DAV4",
    "DAV5",
    "DIA2",
    "DTR2",
    "EGH2",
    "ELA2",
    "ELM2",
    "ELS2",
    "ENG2",
    "FAE2",
    "FIR2",
    "FIS2",
    "FLU2",
    "FNH2",
    "FOU2",
    "FRA2",
    "FRA3",
    "FUL2",
    "FUS2",
    "GAD2",
    "GAN2",
    "GLA2",
    "GOM2",
    "GOM3",
    "GOR2",
    "GOS2",
    "GOS3",
    "GRA2",
    "GUT2",
    "HTR2",
    "HTR3",
    "ILI2",
    "JAU2",
    "JUL2",
    "KES2",
    "KLO2",
    "KLO3",
    "LAG2",
    "LAG3",
    "LAU2",
    "LHO2",
    "LIMP2",
    "LPIL2",
    "LUK2",
    "LUM2",
    "MAE2",
    "MEI2",
    "MES2",
    "MLB2",
    "MTR2",
    "MUN2",
    "MUO2",
    "MUT2",
    "NAR2",
    "NAS2",
    "NEN3",
    "OBM2",
    "OBW2",
    "OBW3",
    "OFE2",
    "ORT2",
    "OTT2",
    "PAR2",
    "PMA2",
    "PMOR2",
    "PRAZ2",
    "PUZ2",
    "RNZ2",
    "ROA2",
    "ROT3",
    "SAA2",
    "SAA4",
    "SAA5",
    "SCA2",
    "SCA3",
    "SCB2",
    "SCH2",
    "SHE2",
    "SIM2",
    "SLF2",
    "SMN2",
    "SPN2",
    "SPN3",
    "STH2",
    "STN2",
    "TAM2",
    "TAM3",
    "TIT2",
    "TRU2",
    "TUJ2",
    "TUJ3",
    "TUM2",
    "URS2",
    "VAL2",
    "VDS2",
    "VIN2",
    "VLS2",
    "VSC2",
    "VST2",
    "WFJ2",
    "YBR2",
    "ZER2",
    "ZER4",
    "ZNZ2",
]
#swiss_metnet_stations = [
 #   "*ABO1",
#    "*ANT1",
#    "*BEH1",
#    "*CDF1",
#    "*CHA1",
#    "*CIM1",
#    "*DAV1",
#    "*DOL1",
#    "*FRE1",
#    "*GEN1",
#    "*GRH1",
#    "*MLS1",
#    "*NAP1",
#    "*ROE1",
#    "*SBE1",
#    "*ULR1",
#    "*ZER1",
#]  ### For the server in the operative live
## For reading from O:/

swiss_metnet_stations = [
    "%2AABO1",
    "%2AANT1",
    "%2ABEH1",
    "%2ACDF1",
    "%2ACHA1",
    "%2ACIM1",
    "%2ADAV1",
    "%2ADOL1",
    "%2AFRE1",
    "%2AGEN1",
    "%2AGRH1",
    "%2AMLS1",
    "%2ANAP1",
    "%2AROE1",
    "%2ASBE1",
    "%2AULR1",
    "%2AZER1",
]
station_list = [
    *imis_stations,
    *swiss_metnet_stations,
]


def get_args_parser():
    parser = argparse.ArgumentParser(
        description="Run predictions with the random forests model"
    )
    parser.add_argument(
        "--date",
        "-d",
        type=lambda x: datetime.strptime(x, date_time_format).strftime(
            date_time_format
        ),
        help="It is validto (last date) for nowcast and "
        "validfrom (first date) for forecast",
        required=True,
    )
    parser.add_argument(
        "--forecast",
        "-f",
        required=False,
        action="store_true",
        help="A flag that triggers predictions using forecast data "
        "in addition to nowcast.",
    )
    return parser
