import os
#from deapsnow_live import config

import config

station_list = config.station_list

path_prefix = os.environ.get("DEAPSNOW_PATH_PREFIX", ".")
path_lib = {
    # Path where rf models are saved after training (i.e. *.sav files)
    "models": f"{path_prefix}/data/models/rf_models/",
    # Path of the forecast data (i.e. .smet, .pro)
    "data_forecast": config.current_season_snowpack_data_dir_forecast,
    # Path to save the input data for the forecast models
    "input_forecast": f"{path_prefix}/../live-data-revamp/rf_model_data/forecast",
    "output_forecast": f"{path_prefix}/../live-data-revamp/predictions/rf_models/forecast",
    # Path of the nowcast data (i.e. .smet, .pro)
    "data_nowcast": config.current_season_snowpack_data_dir_nowcast,
    # Path to save the input data for the nowcast models
    "input_nowcast": f"{path_prefix}/../live-data-revamp/rf_model_data/nowcast",
    "output_nowcast": f"{path_prefix}/../live-data-revamp/predictions/rf_models/nowcast",
}
