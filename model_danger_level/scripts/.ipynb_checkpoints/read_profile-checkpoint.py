"""
@author: Martin Hendrick
"""
import numpy as np
import datetime


def read_file(filename):
    with open(filename) as f:
        content = f.readlines()
    return content


def process_profile_info_single_value(data_type):
    def _process_profile_info_single_value(line):
        return data_type(line.strip().split("=")[1])
    return _process_profile_info_single_value


def process_profile_data_array(data_type, start_ind=None, end_ind=None):
    def _process_profile_array(line):
        return np.array(
            [data_type(x) for x in line.strip().split(",")]
        )[start_ind:end_ind]
    return _process_profile_array


def process_profile_data_timestamp(line):
    return datetime.datetime.strptime(
        line.strip().split(",")[1], "%d.%m.%Y %H:%M:%S"
    )


PROFILE_PARSE_INFO_MAP = {
    # keys are the first 8 character of the line in a .pro file
    "Altitude": {
        "label": "altitude",
        "parse_func": process_profile_info_single_value(float)
    },
    "Latitude": {
        "label": "latitude",
        "parse_func": process_profile_info_single_value(float)
    },
    "Longitud": {
        "label": "longitude",
        "parse_func": process_profile_info_single_value(float)
    },
    "SlopeAng": {
        "label": "slopeAngle",
        "parse_func": process_profile_info_single_value(float)
    },
    "SlopeAzi": {
        "label": "slopeAzi",
        "parse_func": process_profile_info_single_value(float)
    },
    "StationN": {
        "label": "stationName",
        "parse_func": process_profile_info_single_value(str)
    },
}

PROFILE_PARSE_DATA_MAP = {
    # keys are the first 4 character of the line in a .pro file
    "0500": {
        "label": "timestamp",
        "parse_func": process_profile_data_timestamp
    },
    "0501": {
        "label": "height",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0502": {
        "label": "density",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0503": {
        "label": "temperature",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0504": {
        "label": "element_id",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0506": {
        "label": "lwc",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0508": {
        "label": "dendricity",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0509": {
        "label": "sphericity",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0511": {
        "label": "bond_size",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0512": {
        "label": "grain_size",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0513": {
        # last entry is () and 772 is crust
        "label": "grain_type",
        "parse_func": process_profile_data_array(float, 2, -1)
    },
    "0514": {
        "label": "sh_at_surface",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0515": {
        "label": "ice_content",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0516": {
        "label": "air_content",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0517": {
        "label": "stress",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0518": {
        "label": "viscosity",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0520": {
        "label": "temperature_gradient",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0523": {
        "label": "viscous_deformation_rate",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0530": {
        "label": "stab_indices",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0531": {
        "label": "stab_deformation_rate",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0532": {
        "label": "sn38",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0533": {
        "label": "sk38",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0534": {
        "label": "hand_hardness",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0601": {
        "label": "shear_strength",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0604": {
        "label": "ssi",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0606": {
        "label": "crit_cut_length",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0607": {
        "label": "depth_pen",
        "parse_func": process_profile_data_array(float, 2)
    },
    "0540": {
        "label": "date_of_birth",
        "parse_func": process_profile_data_array(str, 2)
    },
}


def process_profile(content, timestamp=None, is3d=False, remove_soil=False, ignore_params=[]):
    prof = {}
    is_data = False
    prof["info"] = {}
    prof["data"] = {}
    ts = None

    for line in content:
        chars_4 = line[:4] if len(line) >= 4 else None
        chars_6 = line[:6] if len(line) >= 6 else None
        chars_8 = line[:8] if len(line) >= 8 else None

        if chars_4 in ignore_params:
            # Parameter chars_4 found in ignore_params, skipping
            continue

        if chars_6 == "[DATA]":
            is_data = True

        try:
            parse_map_val = PROFILE_PARSE_INFO_MAP[chars_8]
        except KeyError:
            if is_data:
                try:
                    parse_map_val = PROFILE_PARSE_DATA_MAP[chars_4]
                except KeyError:
                    continue
            else:
                continue

        if not is_data:
            prof["info"][parse_map_val["label"]] = parse_map_val["parse_func"](line)

        if is_data:
            # parse timestamp
            if chars_4 == "0500":
                ts = parse_map_val["parse_func"](line)
                if ts in prof["data"]:
                    # make sure we do not overwrite data that has already been processed
                    # indicative of things going really wrong with the parsing
                    raise ValueError(f"Overwriting an already processed timestamp at {ts}.")
                prof["data"][ts] = {}
            # parse height
            elif chars_4 == "0501":
                height = parse_map_val["parse_func"](line)
                if len(height) == 1 and height.item() == 0:
                    continue
                else:
                    if parse_map_val["label"] in prof["data"][ts]:
                        # make sure we do not overwrite data that has already been processed
                        # indicative of things going really wrong with the parsing
                        raise ValueError(
                            f"Overwriting an already processed label at {ts} "
                            f"and {parse_map_val['label']}."
                        )
                    prof["data"][ts][parse_map_val["label"]] = height
            # parse any other data value
            else:
                if parse_map_val["label"] in prof["data"][ts]:
                    # make sure we do not overwrite data that has already been processed
                    # indicative of things going really wrong with the parsing
                    raise ValueError(
                        f"Overwriting an already processed label at {ts} "
                        f"and {parse_map_val['label']}."
                    )
                prof["data"][ts][parse_map_val["label"]] = parse_map_val["parse_func"](line)

    if is3d:
        getCoordinates(prof)

    if remove_soil:
        for profile in prof["data"].values():
            try:
                i_gr = np.where((profile["height"] == 0))[0].item()
                profile["height"] = profile["height"][i_gr + 1:]
                profile["density"] = profile["density"][i_gr:]
                profile["temperature"] = profile["temperature"][i_gr:]
                profile["temperature_gradient"] = profile["temperature_gradient"][i_gr:]
                profile["stress"] = profile["stress"][i_gr:]
                profile["ice_content"] = profile["ice_content"][i_gr:]
                profile["air_content"] = profile["air_content"][i_gr:]
                profile["viscosity"] = profile["viscosity"][i_gr:]
                profile["element_ID"] = profile["element_ID"][i_gr:]
                profile["lwc"] = profile["lwc"][i_gr:]
            except:  # noqa
                continue

    for its in prof["data"].keys():
        for var in prof["data"][its].keys():
            data = prof["data"][its][var]
            try:
                prof["data"][its][var] = np.where((data == -999), np.nan, data)
            except:  # noqa
                pass

    if timestamp:
        return prof["data"][timestamp]

    return prof


def getCoordinates(
    prof, yllcorner=186500, xllcorner=779000, gridsize=1, nx=600, ny=600, dem=None
):
    ix, iy, name = prof["info"]["stationName"].strip().split("_")
    ix = int(ix)
    iy = int(iy)
    prof["info"]["ind_x"] = ix
    prof["info"]["ind_y"] = ny - 1 - iy
    prof["info"]["coord_x"] = xllcorner + ix * gridsize
    prof["info"]["coord_y"] = yllcorner + iy * gridsize
    if dem:
        if (
            dem[prof["info"]["ind_y"], prof["info"]["ind_x"]]
            != prof["info"]["altitude"]
        ):
            print("ACHTUNG")
