import numpy as np

#from deapsnow_live.features_from_profile import weakest_pwl
from features_from_profile import weakest_pwl


# min of stability index for layer flaged as pwl=1 (potential weal layer
# i.e. layer/interface havind sum(lemon=True)>=5)


def dry_feat_extract_opt(s):

    s = s.reset_index(drop=True)
    s["depth"] = s.height.max() - s.height  # depth = distance from the top layer

    pwl_100_p = s.loc[
        (s.pwl == 1) & (s.depth < 100)
    ]  # layer with pwl=1 and in the 100cm from the top
    if (
        len(pwl_100_p) > 0
    ):  # is there a pwl in the 100cm from the top? yes pwl_100 = 1 no pwl_100=0
        pwl_100 = 1
    else:
        pwl_100 = 0

    pwl_100_15_p = s.loc[(s.pwl == 1) & (s.depth < 100) & (s.depth > 15)]
    if (
        len(pwl_100_15_p) > 0
    ):  # is there a pwl in the 15-100cm from the top? yes pwl_100_15 = 1 no pwl_100_15=0
        pwl_100_15 = 1
    else:
        pwl_100_15 = 0

    s_pwl = s.loc[s.pwl == 1]
    min_depth_pwl = s_pwl.depth.min()  # depth of closest pwl from the top layer
    max_depth_pwl = s_pwl.depth.max()  # depth of the deepest pwl from the trop layer
    mean_depth_pwl = s_pwl.depth.mean()  # mean depth of the pwl from the trop layer
    s_100_15 = s.loc[(s.depth < 100) & (s.depth > 15)].copy()
    s_100_15["ccl_sk38"] = (
        s_100_15["sk38"] * s_100_15["crit_cut_length"]
    )  # experimental stability index: sk38*crit_cut_length in the 15-100cm
    min_ccl_sk38 = s_100_15[
        "ccl_sk38"
    ].min()  # min of sk38*crit_cut_length in the 15-100cm
    if len(s_100_15) > 0:
        min_ccl_sk38_ind = s_100_15["ccl_sk38"].idxmin()
        j = s_100_15.loc[s_100_15.index == min_ccl_sk38_ind]
        min_ccl_sk38_depth = (
            j.depth.mean()
        )  # .item()						#depth of the min of sk38*crit_cut_length in the 15-100cm
    else:
        min_ccl_sk38_depth = np.nan

    base = s.loc[(s.height < 30)]
    if len(base.loc[base.pwl == 1]) > 0:
        base_pwl = 1  # is there a pwl in the 30cm from the ground? yes: base_pwl = 1, no:base_pwl=0
    else:
        base_pwl = 0
    list_feat = ["ssi", "sk38", "sn38", "crit_cut_length"]
    r = weakest_pwl(s, list_feat)
    return (
        pwl_100,
        pwl_100_15,
        min_depth_pwl,
        min_ccl_sk38,
        base_pwl,
        min_ccl_sk38_depth,
        max_depth_pwl,
        mean_depth_pwl,
        r,
    )
