set_prof = [
    "crit_cut_length_weak_100",
    "min_ccl_pen",
    "pen_depth",
    "sn38_weak_100",
]

set1_smet = [
    "HN24_mean",
    "HN24_mean_7d",
    "HN72_24_mean",
    "HS_mod_mean",
    "ILWR_mean",
    "ISWR_dir_mean",
    "LWR_net_mean",
    "MS_Snow_mean",
    "Qg0_mean",
    "Qs_mean",
    "Qw_mean",
    "RH_mean",
    "S4_mean",
    "Sn_mean",
    "Ss_mean",
    "TSS_mod_mean",
    "TA_mean",
    "VW_drift_mean",
    "VW_mean",
    "pAlbedo_mean",
    "wind_trans24_mean",
    "wind_trans24_mean_3d",
    "wind_trans24_mean_7d",
    "zSn_mean",
]

set2_smet = [
    "HN24_mean",
    "HN24_mean_7d",
    "HN72_24_mean",
    "HS_mod_mean",
    "ILWR_mean",
    "ISWR_dir_mean",
    "LWR_net_mean",
    "MS_Snow_mean",
    "Qg0_mean",
    "Qs_mean",
    "Qw_mean",
    "RH_mean",
    "S4_mean",
    "Sn_mean",
    "Ss_mean",
    "TA_mean",
    "TS2_mean",
    "TSS_mod_mean",
    "TS0_mean",
    "VW_drift_mean",
    "VW_mean",
    "pAlbedo_mean",
    "wind_trans24_mean",
    "wind_trans24_mean_3d",
    "wind_trans24_mean_7d",
    "zSn_mean",
]


set1 = [
    *set1_smet,
    *set_prof,
]

set2 = [
    *set2_smet,
    *set_prof,
]


### New models run with set3

set3_smet = ["HN24_mean",
             "HN24_mean_7d",
             "HN72_24_mean",
             "HS_mod_mean",
             "ILWR_mean",
             "ISWR_diff_mean",
             "LWR_net_mean",
             "MS_Snow_mean",
             "MS_Water_mean",
             "OLWR_mean",
             "OSWR_mean",
             "Qg0_mean",
             "Qs_mean",
             "Qw_mean",
             "RH_mean",
             "S4_mean",
             "S5_mean",
             "Sn_mean",
             "Ss_mean",
             "TA_mean",
             "VW_drift_mean",
             "VW_mean",
             "pAlbedo_mean",
             "wind_trans24_mean",
             "wind_trans24_mean_3d",
             "wind_trans24_mean_7d",
             "zSn_mean"]

set3_prof = [
    "min_ccl_pen",
    "pen_depth",
    "sn38_weak_100",
]

set3 = [
    *set3_smet,
    *set3_prof,
]


set4_smet = [
    "HN24_mean",
    "HN24_mean_7d",
    "HN72_24_mean",
    "HS_mod_mean",
    "ILWR_mean",
    "ISWR_dir_mean",
    "LWR_net_mean",
    "MS_Snow_mean",
    "Qg0_mean",
    "Qs_mean",
    "Qw_mean",
    "RH_mean",
    "S4_mean",
    "Sn_mean",
    "Ss_mean",
    "TA_mean",
    "TS2_mean",
    "TSS_mod_mean",
    "TS0_mean",
    "VW_drift_mean",
    "VW_mean",
    "pAlbedo_mean",
    "wind_trans24_mean",
    "wind_trans24_mean_3d",
    "wind_trans24_mean_7d",
    "zSn_mean",
    "ISWR_diff_mean", 
    "MS_Water_mean", 
    "OLWR_mean", 
    "OSWR_mean", 
    "S5_mean"]

set4 = [
    *set4_smet,
    *set_prof,
]