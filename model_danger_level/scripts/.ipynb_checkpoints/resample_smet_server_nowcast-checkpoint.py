# -*- coding: utf-8 -*-
"""
Created: 23/11/2020
1) Read station.smet or station.ext.smet data from the server

2) Resample and computation of the mean of all the 'smet' features in a 24h window
(from 'validfrom' --> 'validto' dates):
      # validto = last recording date of the file (for example: '2020-11-24 21:00:00').
        Date & time of the nowcast
      # validfrom = validto - 1 day + 3h  (for example: '2020-11-24 00:00:00')

3) Computation of two new 'smet' feaures:
      # 'HN24_mean_7d': Sum of the 7 days, 24 hours mean of 'HN24'
      # 'wind_trans24_mean_7d': Sum of the 7 days, 24 hours mean of 'wind_trans24'
      # 'wind_trans24_mean_3d': Sum of the 3 days, 24 hours mean of 'wind_trans24'
@author: Cristina Perez
"""

import numpy as np
import pandas as pd
from pathlib import Path
import datetime

#from deapsnow_live import config
#from deapsnow_live.parsing.smet import read_smet

import config
from smet import read_smet


def read_resample_smet_nowcast(fin, datetime_str):
    """
    fin = Path of the nowcast dataset
    datetime_st = nowcast date (input validto date of the nowcast)
    """
    if not isinstance(fin, Path):
        fin = Path(fin)
    df = read_smet(fin)
    filename = fin.name

    # Computation of the mean of all features in a 24h window
    # (from 'validfrom' - to 'validto' dates)
    # validfrom = (df['timestamp'].max() - pd.Timedelta(days=1) + pd.Timedelta(hours=3))
    # validto = df['timestamp'].max()
    validto = datetime.datetime.strptime(datetime_str, config.date_time_format)
    validfrom = validto - pd.Timedelta(days=1) + pd.Timedelta(hours=3)
    df_res = df.copy()  # df_res = resampled dataframe
    df_res = df_res[
        (df_res["timestamp"] >= validfrom) & (df_res["timestamp"] <= validto)
    ]
    time_min = df_res['timestamp'].min()
    time_max = df_res['timestamp'].max()
    df_res["validfrom"] = validfrom
    df_res["validto"] = validto
    df_res = df_res.groupby(["validfrom", "validto"]).agg(["mean"])
    df_res.columns = ["_".join(x) for x in df_res.columns.ravel()]
    df_res.reset_index(inplace=True)

    # Computation of 3 and 7 days sum of daily mean of ['HN24', 'wind_trans24']
    # from (validto - 7 days) to (validto)
    df2 = df.copy()
    df2 = df2[df2["timestamp"] <= validto]
    df2[["datum", "time"]] = (
        df2["timestamp"].astype(str).str.split(" ", n=1, expand=True)
    )  # New colunms for resampling & Sum
    df2.loc[df2["time"] != df2["time"].iloc[-1], ["datum", "time"]] = np.nan
    df2[["datum", "time"]] = df2[["datum", "time"]].fillna(method="bfill")
    df2["datum"] = pd.to_datetime(df2["datum"])
    df2 = df2[["datum", "HN24", "wind_trans24"]]
    df3 = df2[["datum", "wind_trans24"]].copy()

    # Resampling 7 days (df2) & 3 days (df3):
    df2 = (
        df2[["datum", "HN24", "wind_trans24"]]
        .set_index("datum")
        .resample(rule="1d")
        .mean()
        .rolling(window="7d", min_periods=7, closed="right")
        .sum()
        .rename(
            columns={"HN24": "HN24_mean_7d", "wind_trans24": "wind_trans24_mean_7d"}
        )
    )

    df3 = (
        df3[["datum", "wind_trans24"]]
        .set_index("datum")
        .resample(rule="1d")
        .mean()
        .rolling(window="3d", min_periods=3, closed="right")
        .sum()
        .rename(columns={"wind_trans24": "wind_trans24_mean_3d"})
    )

    # Add the three new features & station_code
    df_res["HN24_mean_7d"] = df2["HN24_mean_7d"].iloc[-1]
    df_res["wind_trans24_mean_7d"] = df2["wind_trans24_mean_7d"].iloc[-1]
    df_res["wind_trans24_mean_3d"] = df3["wind_trans24_mean_3d"].iloc[-1]
    df_res['time_min'] = time_min
    df_res['time_max'] = time_max
    df_res["station_code"] = filename[0:4]

    return df_res
