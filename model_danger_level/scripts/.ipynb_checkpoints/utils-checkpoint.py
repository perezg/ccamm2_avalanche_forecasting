"""
@author: Martin Hendrick
"""
from urllib.parse import urljoin
from urllib.request import urlopen

from read_profile import process_profile, read_file
from features_from_profile import features_from_profile_datetime


def from_file(file_name, date_time):
    """
    file name should be something like WFJ2.pro
    date_time format should be like '2021-09-20 18:00:00'
    """
    lines = read_file(file_name)
    pro_df = process_profile(lines)
    profile_features = features_from_profile_datetime(pro_df, file_name, date_time)
    return profile_features


def from_server(file_name, date_time, url='http://snowpack.wd.op/share/snowpack-imis/output/'):
    """
    file name should be something like WFJ2.pro
    date_time format should be like '2021-09-20 18:00:00'
    """
    target_url = urljoin(url, file_name)
    u = urlopen(target_url)
    data = u.read()
    deco_data = data.decode('ISO-8859-1').splitlines()
    pro_df = process_profile(deco_data)
    profile_features = features_from_profile_datetime(pro_df, file_name, date_time)
    return profile_features
